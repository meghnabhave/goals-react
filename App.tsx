import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { Provider } from 'react-redux'
import { store } from './src/redux/app-redux';

import LoginScreen from './src/screens/LoginScreen';
import SignupScreen from './src/screens/SignupScreen';
import DashboardScreen from './src/screens/DashboardScreen';
import AddItemFormScreen from './src/components/add-item/AddItemFormScreen';
import GoalsScreen from './src/screens/GoalsScreen';
import TasksScreen from './src/screens/TasksScreen';
import AllTasksScreen from './src/screens/AllTasksScreen';
import AgendaScreen from './src/screens/AgendaScreen';
import ForgotPasswordScreen from './src/screens/ForgotPasswordScreen';
import ProfileScreen from './src/screens/ProfileScreen';
import CheckIfLoggedInLoader from './src/screens/CheckIfLoggedInLoader';
import GenericAddFormScreen from './src/components/add-item/GenericAddFormScreen';

const Stack = createStackNavigator();

function headerStyleOptions(title: string) {
  return {
    title: title,
    headerStyle: {
      backgroundColor: '#ffffff',
      borderWidth: 0,
      elevation: 0,
      shadowOpacity: 0,
      borderBottomWidth: 0,
    },
    headerTintColor: '#DF2E2E',
    headerTitleStyle: {
      fontWeight: 'normal',
      textAlign: 'center',
      alignSelf:'center'
    }

  }
}

function App() {
  return (
    <Provider store={store}>
        <NavigationContainer theme={{colors: {background: '#FFFFFF'}}}>
          <Stack.Navigator>
            <Stack.Screen name="CheckIfLoggedInLoader" component={CheckIfLoggedInLoader}
              options={headerStyleOptions('GOALS')}/>
            <Stack.Screen name="Login" component={LoginScreen}
              options={headerStyleOptions('GOALS')}/>
            <Stack.Screen name="Signup" component={SignupScreen}
              options={headerStyleOptions('SIGNUP')}/>
            <Stack.Screen name="GoalsOverview" component={DashboardScreen}
              options={headerStyleOptions('GOALS OVERVIEW')}/>
            <Stack.Screen name="AllTasks" component={AllTasksScreen}
              options={headerStyleOptions('TASKS OVERVIEW')}/>
            <Stack.Screen name="Agenda" component={AgendaScreen}
              options={headerStyleOptions('AGENDA')}/>
            <Stack.Screen name="AddItemForm" component={AddItemFormScreen}
              options={headerStyleOptions('ADD FORM')}/>
            <Stack.Screen name="Goals" component={GoalsScreen}
              options={headerStyleOptions('GOAL')}/>
            <Stack.Screen name="Tasks" component={TasksScreen}
              options={headerStyleOptions('TASK')}/>
            <Stack.Screen name="ForgotPassword" component={ForgotPasswordScreen}
              options={headerStyleOptions('FORGOT PASSWORD')}/>
            <Stack.Screen name="Profile" component={ProfileScreen}
              options={headerStyleOptions('PROFILE')}/>
            <Stack.Screen name="GenericAddForm" component={GenericAddFormScreen}
              options={headerStyleOptions('CREATE')}/>
          </Stack.Navigator>
        </NavigationContainer>
              </Provider>
  );
}

export default App;
