export enum ItemType {
  GOAL,
  TASK,
  SUBTASK
}

export enum State {
  OPEN = "Open",
  PROGRESS = "In Progress",
  TESTING = "In Testing",
  DONE ="Done"
}

export enum Place {
  BACKLOG = "Backlog",
  AGENDA = "Agenda"
}

export class PlaceHistory {
  sprintNumber: number;
  startDateOfSprint: Date;
  endDateOfSprint: Date;
}

export class Item {
  key: string;
  name: string;
  type: ItemType;
  subItems: Item[]; // array of items keys or empty
  creationTimestamp: string;
  state: State;
  parentKey: string;
  progress: number;
  place: Place = Place.BACKLOG;
  placeHistory: PlaceHistory[];
}
