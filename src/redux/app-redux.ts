import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { usersRef } from '../config';

//
// Initial State...
//
const initialState = {
  userData: {
    email: '',
    username: '',
    list: []
  },
}

//
// Reducer...
//
const reducer = (state = initialState, action) => {
switch(action.type) {
      case "setUserData":
          return { ...state, userData: action.value };

      default:
          return state;
  }
}

//
// Store...
//
const store = createStore(reducer, applyMiddleware(thunkMiddleware));
export { store };

//
// Action Creators...
//

const setUserData = (userData) => {
    return {
        type: "setUserData",
        value: userData
    };
}

const watchUserData = (uid) => {
    return function(dispatch) {
      usersRef
      .child(uid)
      .on("value", function(snapshot) {
          var userData= getUserDataFromSnapshot(snapshot);
          console.log('in action');
          console.log(userData);
          console.log('after action');
          dispatch(setUserData(userData));
      }, function(error) {
            console.log(err);
      });
    };
}

export { watchUserData };

//
// Helper Functions
//

function getUserDataFromSnapshot(snapshot) {
    var userData = snapshot.val();
    var list = [];

    for (const [key, object] of Object.entries(userData.list)) {
      if (key != null) {
        object.key = key;
        list.push(object);
      }
    }

    return {
      username: userData.username,
      email: userData.email,
      list: list
    };

}
