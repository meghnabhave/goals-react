import { auth, usersRef } from '../config';

class DatabaseUtils {

  //
  // Data Updates
  //

  static storeItem(uid, itemType, parentKey, data) {
    let newItemRef = usersRef.child(uid).child('list').push(data);

    if(itemType != 'Goals'){
      usersRef.child(uid + '/list/' + parentKey + '/subItems').push(newItemRef.key);
    }

    return newItemRef;
  }

  static updateItem(uid, itemKey, item) {
    usersRef
    .child(uid + "/list/" + itemKey).update(item);
  }

  static deleteItem(uid, itemKey, itemParentKey) {

    let subItemsRef = usersRef.child(uid + "/list/" + itemKey + "/subItems");

    var itemPromise =
    new Promise((resolve, reject) => {

      try{
        subItemsRef
        .on("value", function(snapshot) {
            let subItems = getAllSubItems(snapshot);
            resolve(subItems);
          });
      } catch(err) {
        console.log(err);
        reject('In delete item: subItems get data failed.');
      }

    });

    itemPromise.then(function(subItems) {
      if ( subItems.length === 0) {

      } else {
        subItems.forEach(function(subItem) {
          DatabaseUtils.deleteItem(uid, subItem.value, itemKey);
        });
      }

    }, function(reject) {
      console.log(reject);
    });

    usersRef.child(uid + "/list/" + itemKey).remove();


    if( itemParentKey === null ){
    } else {
      console.log('Deleting parent subitems...');
      let parentSubItemsRef = usersRef.child(uid + "/list/" + itemParentKey + "/subItems");

      var parentPromise =
      new Promise((resolve, reject) => {

        try{
          parentSubItemsRef
          .on("value", function(snapshot) {
              let childKey = getChildKeyToDelete(snapshot, itemKey);
              console.log('In new promise: ', childKey);
              resolve(childKey);
            });
        } catch(err) {
          console.log(err);
          reject('In delete item: parents subitems get data api failed');
        }

      });

      parentPromise.then(function(childKey){
        console.log('In promise then: ', childKey);
        parentSubItemsRef
        .child(childKey)
        .remove();
        console.log('Removed subItm from parent...');
      }, function(reject) {
        console.log(reject);
      });
    }

    return;
  }

  //
  // Auth
  //

  static checkLoginStatus() {
       return new Promise((resolve, reject) => {
        try {
          auth
           .onAuthStateChanged(user => {
              console.log(user.email);
               resolve(user);
           });
        } catch {
          reject('check login status: api failed')
        }
      });
  }

  static checkIfAlreadyLoggedInAndNavigateToDashboard(navigation) {
    console.log('Check if already logged in.');
    auth.onAuthStateChanged(user => {
        console.log('user: ', user);
        if (user) {
          navigation.replace('Agenda', {user});
        } else {
          navigation.replace('Login');
        }
    });
  }

  static signInWithEmailAndPassword(email, password, navigation) {
    console.log('Sign in:', email, password);
    return auth.signInWithEmailAndPassword(email, password);
  }

  static forgotPassword(email) {
    return auth.sendPasswordResetEmail(email);
  }

  static createUserWithEmailAndPassword(email, password) {
    return auth.createUserWithEmailAndPassword(email, password);
  }

  static updateProfile(detail) {
    return auth.onAuthStateChanged(user => {
      user.updateProfile(detail);
    })
  }

  static signOut(navigation) {
    auth.signOut().then(function() {
      // Sign-out successful.
      console.log('Signout successful');
      navigation.navigate('Login');
    }).catch(function(error) {
      console.log(error);
    });
  }

  static addUserToRealtimeDatabase(uid, newUser) {
    usersRef.child(uid).set(newUser);
  }

}

function getAllSubItems(snapshot) {
  // setProgress(snapshot);
  var array = [];

  snapshot.forEach(function(childSnapshot){
    item = {
      value: childSnapshot.val(),
      key: childSnapshot.key
    }

    array.push(item);
  });

  return array;
}

function getChildKeyToDelete(snapshot, childKeyValue) {
  var returnKey = "";
  snapshot.forEach(function(childSnapshot) {
    if(childKeyValue === childSnapshot.val()) {
      returnKey = childSnapshot.key;
    }

  });

  console.log('in snapshot foreach', returnKey);

  return returnKey;
}


export default DatabaseUtils;
