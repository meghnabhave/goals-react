import React from 'react';
import { Text } from 'react-native';

import PropTypes from 'prop-types';
import AddItemButton from '../add-item/AddItemButton';
import ItemList from '../list-components/ItemList';

;import DatabaseUtils from '../../database/DatabaseUtils';
import { Item, ItemType, State, Place } from '../../datamodel/Item';

import { connect } from 'react-redux';
import { watchUserData } from '../../redux/app-redux';

const mapStateToProps = (state, ownProps) => {
  return {
    list: getItems(state.userData.list, ownProps.itemType, ownProps.itemType === 'Goals' ? "null" : ownProps.item.key)
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    watchUserData: (uid) => dispatch(watchUserData(uid))
  };
}

class ItemDetailsScreen extends React.Component {

  constructor(props) {
    super(props);
    this.props.watchUserData(this.props.user.uid);
  }

  render() {
    let  item  = {
      name : 'Dashboard'
    };
    if(this.props.itemType == 'Goals') {

    } else {
      item.name = this.props.item.name;
    }

    return [
      <Text></Text>,
      <ItemList
        itemType={this.props.itemType}
        itemSubtype={this.props.itemSubtype}
        data={this.props.list}
        user={this.props.user}
        navigation={this.props.navigation}/>,
      <AddItemButton
        item={this.props.itemType}
        returnData={addDataToState.bind(this)}
        navigation={this.props.navigation}/>,
    ]

  }
}

function addDataToState(data: Item) {

  data.state = State.OPEN;
  data.progress = 0.0;
  data.place = Place.BACKLOG;
  data.creationTimestamp = new Date();
  switch(this.props.itemType) {
    case 'Goals':
        data.type = ItemType.GOAL;
        data.parentKey = 'null';
        data.subItems = [];
      break;
    case 'Tasks':
        data.type = ItemType.TASK;
        data.subItems = [];
        data.parentKey = this.props.item.key;
      break;
    case 'Subtasks':
        data.type = ItemType.SUBTASK;
        data.subItems = [];
        data.parentKey = this.props.item.key;
      break;
  }

  DatabaseUtils.storeItem(this.props.user.uid, this.props.itemType, this.props.itemType != 'Goals'? this.props.item.key : null, data);

}

ItemDetailsScreen.propTypes = {
  itemType: PropTypes.string,
  itemSubtype: PropTypes.string,
  user: PropTypes.object,
  navigation: PropTypes.object
}

function getItems(list, type, key) {
  // setProgress(snapshot);
  var returnArr = [];
  list.forEach(function(item) {
      if( item.place === null || item.place === undefined ) {
        item.place = Place.BACKLOG;
      }
      switch(type) {
        case 'Goals':
            if (item.type === ItemType.GOAL && item.parentKey === key) {
              returnArr.push(item);
            }
          break;
        case 'Tasks':
            if (item.type === ItemType.TASK && item.parentKey === key) {
              returnArr.push(item);
            }
          break;
        case 'Subtasks':
            if (item.type === ItemType.SUBTASK && item.parentKey === key) {
              returnArr.push(item);
            }
          break;
      }

  });
  return returnArr;
};

export default connect(mapStateToProps, mapDispatchToProps)(ItemDetailsScreen);
