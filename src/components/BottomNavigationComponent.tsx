import * as React from 'react';
import { View } from 'react-native';
import { Button } from 'react-native-paper';

const BottomNavigationComponent = ({navigation, user}) => {
  return (
    <View style={{flexDirection: 'row', position: 'absolute', bottom: 1, marginBottom: 10, marginRight: 10, marginLeft: 10, borderWidth:1, borderStyle:'solid', borderColor: '#D4D4D4'}}>
      <Button mode='text' style={{flex:3}} contentStyle={{backgroundColor: '#ffffff'}} color={'black'} onPress={() => navigation.navigate('GoalsOverview',{ user : user })}>Overview</Button>
      <Button mode='text' style={{flex:1}} contentStyle={{backgroundColor: '#ffffff'}} color={'black'} onPress={() => navigation.navigate('GenericAddForm',{ user : user })}>+</Button>
      <Button mode='text' style={{flex:3}} contentStyle={{backgroundColor: '#ffffff'}} color={'black'} onPress={() => navigation.navigate('AllTasks',{ user : user })}>Planner</Button>
    </View>
  );
};

export default BottomNavigationComponent;
