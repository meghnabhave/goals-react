import React from 'react';
import { StyleSheet } from 'react-native';
import { FAB } from 'react-native-paper';

import {
  TouchableOpacity,
  Text
} from 'react-native';

import PropTypes from 'prop-types';

function AddItemButton(props) {
  return(
    <FAB
    style={styles.fab}
    color={'#FFFFFF'}
    small
    icon="plus"
    onPress={() => handleAddEvent(props)}
    />

  )
}

function handleAddEvent(props) {
  console.log("Action button pressed!");
  props.navigation.navigate('AddItemForm',
    {returnData: props.returnData, item:props.item});
}

const styles = StyleSheet.create({
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
    backgroundColor: '#DF2E2E'
  },
})

AddItemButton.propTypes = {
  item: PropTypes.string,
  returnData: PropTypes.func,
  navigation: PropTypes.object
}

export default AddItemButton;
