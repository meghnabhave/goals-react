import React from 'react';

import {
  Text,
  TextInput,
  View,
  TouchableOpacity,
  StyleSheet
} from 'react-native';

class AddItemFormScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      name: 'No Goal'
    }
  }

  render() {
    const type = this.props.route.params.item.trim().slice(0,-1);
    return (
      <View style={styles.container}>
        <Text style={{padding: 10, marginBottom: 10}}>Add a {type}, and check on Overview page to view them.</Text>
        <View style={styles.inputView}>
          <TextInput
            style={styles.input}
            placeholder={type}
            onChangeText={
              text => this.setState({ name: text })
            }
            >
          </TextInput>
        </View>

        <TouchableOpacity
            style={styles.button}
            onPress={handleAddGoal.bind(this)}
          >
          <Text style={styles.buttonText}>Add {type}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
  },
  button : {
    width:"80%",
    height:60,
    backgroundColor: '#333',
    padding:20,
    justifyContent: 'center',
    alignItems: 'center'
  },

  buttonText : {
    color: '#FAFAFA',
    fontSize: 20,
    fontWeight: '600'
  },
  inputView : {
    width:"80%",
    backgroundColor:"#FAFAFA",
    height:50,
    marginBottom:20,
    padding:20,
    justifyContent: 'center',
    alignItems: 'center'
  },

  input : {
    height:50,
    color:'black',
  },
})

function handleAddGoal() {
  this.props.route.params.returnData(this.state);
  this.props.navigation.pop();
}

export default AddItemFormScreen;
