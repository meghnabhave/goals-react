import React from 'react';

import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import { TextInput, Snackbar } from 'react-native-paper';
import { Dropdown } from 'react-native-material-dropdown';
import { Item, ItemType, State, Place } from '../../datamodel/Item';
import DatabaseUtils from '../../database/DatabaseUtils';

import { connect } from 'react-redux';
import { watchUserData } from '../../redux/app-redux';

const POSSIBLE_STATES = [
  { value: "Goal" },
  { value: "Task" },
  { value: "Subtask" }
]

const mapStateToProps = (state) => {
  return {
    data: getGoalNameAndKey(state.userData.list)
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    watchUserData: (uid) => dispatch(watchUserData(uid))
  };
}

class GenericAddFormScreen extends React.Component {

  constructor(props) {
    super(props);
    this.props.watchUserData(this.props.route.params.user.uid);

    this.state = {
      title: null,
      type: ItemType.GOAL,
      description: null,
      goal: {
        key: null,
        name: null
      },
      task: {
        key: null,
        name: null,
      }
    }
  }



  getGoalNames() {
    return this.props.data.map(item => ({
      value: item.goalName,
      key: item.goalKey
    }));
  }

  getTaskNames(goalKey) {
    if (goalKey !== null) {
      return this.props.data.find(item => item.goalKey === goalKey).tasks.map(task => ({
        value: task.taskName,
        key: task.taskKey
      }));
    } else {
      return [];
    }
  }

  getGoalDetails(goalName) {
    let key = this.getGoalNames().filter(goals => goals.value === goalName)[0].key;
    return {
      key: key,
      name: goalName
    }
  }

  getTaskDetails(taskName) {
    let key = this.getTaskNames().filter(tasks => tasks.value === taskName)[0].key;
    return {
      key: key,
      name: taskName
    }
  }

  render() {
    return (
      <View style={ styles.form }>
        <Text>Add a Goal, Task or Subtask.</Text>
        <Text>Go to Planner to add it to current agenda.</Text>
        <View
            style = { styles.inputView } >
          <TextInput
            mode= 'outlined'
            theme={{colors: {primary: '#4D4E4F'}}}
            style={ styles.input }
            placeholder="Title"
            autoCapitalize='none'
            error ={false}
            onChangeText={
              text => this.setState(prevState => ({
                ...prevState,
                title: text,
                goal: {
                  ...prevState.goal
                },
                task: {
                  ...prevState.task
                }
              }))
            }
            value={this.state.title}
          />
        </View>
        <View
            style = { styles.inputView } >
          <Dropdown
              label='Type'
              data={POSSIBLE_STATES}
              value='Goal'
              containerStyle={{backgroundColor:'#ffffff', padding:5, marginTop: 5, marginBottom: 5, borderRadius:5, borderWidth:1, borderStyle:'solid', borderColor: '#ACA5A5'}}
              onChangeText={text => this.setState(prevState => ({
                ...prevState,
                type: getItemType(text),
                goal: {
                  ...prevState.goal
                },
                task: {
                  ...prevState.task
                }
              }))}
          />
        </View>
        {this.state.type === ItemType.TASK || this.state.type === ItemType.SUBTASK ? (
          <>
            <View
                style = { styles.inputView } >
                <Dropdown
                    label='Goal Name'
                    data={this.getGoalNames()}
                    containerStyle={{backgroundColor:'#ffffff', padding:5, marginTop: 5, marginBottom: 5, borderRadius:5, borderWidth:1, borderStyle:'solid', borderColor: '#ACA5A5'}}
                    onChangeText={text => this.setState(prevState => ({
                      ...prevState,
                      goal: this.getGoalDetails(text),
                      task: {
                        ...prevState.task
                      }
                    }))}
                />
            </View>
            {this.state.goal !== null && this.state.type === ItemType.SUBTASK ? (
              <View
                  style = { styles.inputView } >
                  <Dropdown
                      label='Task Name'
                      data={this.getTaskNames(this.state.goal.key)}
                      containerStyle={{backgroundColor:'#ffffff', padding:5, marginTop: 5, marginBottom: 5, borderRadius:5, borderWidth:1, borderStyle:'solid', borderColor: '#ACA5A5'}}
                      onChangeText={text => this.setState(prevState => ({
                        ...prevState,
                        goal: {
                          ...prevState.goal,
                        },
                        task: this.getTaskDetails(text),
                      }))}
                  />
              </View>
            ) : null }
          </>
        ) : null}
        <TouchableOpacity
            style={styles.button}
            onPress={() => {
              console.log(this.state);
              addDataToState(this.state, this.props.route.params.user.uid, this.props.navigation);
            }
          }
          >
          <Text style={styles.buttonText}>Submit</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  form : {
    flex: 1,
    backgroundColor: '#ffffff',
    alignItems: 'center',
  },

  inputView : {
    width:"80%"
  },

  inputHeight: {
    height: 100,
  },

  input : {
    height:50,
    backgroundColor: "#ffffff",
    borderColor:"#ACA5A5",
    fontSize: 15

  },

  button : {
    width:"80%",
    height:60,
    backgroundColor: '#333',
    padding:20,
    justifyContent: 'center',
    alignItems: 'center'
  },

  buttonText : {
    color: '#FAFAFA',
    fontSize: 20,
    fontWeight: '600'
  },
});

function addDataToState(state, uid, navigation) {
  let snackMessage: string = "";
  let data: Item = {};
  data.name = state.title;
  data.description = state.description;
  data.state = State.OPEN;
  data.progress = 0.0;
  data.place = Place.BACKLOG;
  data.creationTimestamp = new Date();
  data.type = state.type;
  switch(state.type) {
    case ItemType.GOAL:
        data.parentKey = 'null';
        data.subItems = [];
        snackMessage = 'Added a goal, go to Overview tab to view them.'
      break;
    case ItemType.TASK:
        data.subItems = [];
        data.parentKey = state.goal.key;
        snackMessage = "Added a Task, go to Planner view to add it to current agenda."
      break;
    case ItemType.SUBTASK:
        data.subItems = [];
        data.parentKey = state.task.key;
        snackMessage = "Added a subtask.";
      break;
  }

  DatabaseUtils.storeItem(uid, state.type, data.parentKey, data);
  navigation.goBack();
}

function getItemType(text: string): ItemType {
  switch(text) {
    case "Goal": return ItemType.GOAL; break;
    case "Task": return ItemType.TASK; break;
    case "Subtask": return ItemType.SUBTASK; break;
  }
}

function getGoalNameAndKey(list) {
  var arrayOfItems = [];
  list.forEach(function(item) {
    arrayOfItems.push(item);
  });

  var returnArr = [];
  // {
  //    goalName:
  //    goalKey:
  //    tasks: [
  //      {
  //        taskName:
  //        taskKey:
  //        subtask: [
  //          {
  //            subtaskName:
  //            subtaskKey:
  //          }
  //        ]
  //      }
  //    ]
  // }
  returnArr = arrayOfItems
                .filter(item => item.type === ItemType.GOAL)
                .map(item => ({
                    goalName: item.name,
                    goalKey: item.key,
                    tasks: []
                }));

   returnArr.forEach(returnItem => {
     returnItem.tasks = arrayOfItems
                            .filter(item => item.type === ItemType.TASK && item.parentKey === returnItem.goalKey)
                            .map(item => ({
                              taskName: item.name,
                              taskKey: item.key,
                              subtasks: []
                            }));
      returnItem.tasks.forEach(task => {
        task.subtasks = arrayOfItems
                          .filter(item => item.type === ItemType.SUBTASK && item.parentKey === task.taskKey)
                          .map(item => ({
                              subtaskName: item.name,
                              subtaskKey: item.key
                          }));
      });
   });
   return returnArr;
}

export default connect(mapStateToProps, mapDispatchToProps)(GenericAddFormScreen);
