import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text
} from 'react-native';

import { TextInput, Button, Menu } from 'react-native-paper';
import { Dropdown } from 'react-native-material-dropdown';
import Item from '../../datamodel/Item';

function editField(field, text) {
    let item = this.state.item;
    item[field] = text;
    this.setState({item});
}

class ModalEditForm extends React.Component {

  // key: string;
  // name: string;
  // type: ItemType;
  // subItems: Item[]; // array of items keys or empty
  // creationTimestamp: string;
  // state: State;
  // parentKey: string;
  // progress: number;
  // place: Place = Place.BACKLOG;
  // placeHistory: PlaceHistory[];

  constructor(props) {
    super(props);
    this.state = {
      name : this.props.prevData.name,
      state: this.props.prevData.state
    }

     this.possibleStates = [{
       value:"Open"
     },
     {
       value:"In Progress"
     },

     {
       value:"In Testing"
     },

     {
       value:"Done"
     }];
  }



render() {

  return (
    <View style={styles.container}>
     <TextInput
       label='Name'
        value={this.state.name}
        onChangeText={text =>
          this.setState({ name : text })
        }
      />

      <Dropdown
              label='State'
              value={this.state.state}
              data={this.possibleStates}
              containerStyle={{backgroundColor:'#f1f1f1', padding:10}}
              onChangeText={text =>
                this.setState({ state : text })
              }
            />

    <Button icon="save" mode="contained" onPress={() => this.props.editDataHandler(this.state)}>
      Submit
    </Button>
  </View>
  );
}

}

const styles = StyleSheet.create({
  container: {
      paddingTop: 23
   },
   input: {
      margin: 15,
      height: 40,
      borderColor: '#7a42f4',
      borderWidth: 1,
      padding: 10,
      alignSelf: 'stretch'
   },
   submitButton: {
      backgroundColor: '#7a42f4',
      padding: 10,
      margin: 15,
      height: 40,
      alignSelf: 'stretch'
   },
   submitButtonText:{
      color: 'white'
   }
})

export default ModalEditForm;
