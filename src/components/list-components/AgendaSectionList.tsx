import React from 'react';
import { Text, FlatList, StyleSheet, View } from 'react-native';
import { IconButton } from 'react-native-paper';
import { State } from '../../datamodel/Item';
import Swipeout from 'react-native-swipeout';

const STATES = [
  {
    currState: State.OPEN,
    nextState: State.PROGRESS,
    buttonDisplayText: 'Start Progress',
    backgroundColor: '#D4D4D4',
    color: 'black',
  },
  {
    currState: State.PROGRESS,
    nextState: State.TESTING,
    buttonDisplayText: 'Start Testing',
    backgroundColor: '#D4D4D4',
    color: 'black',
  },
  {
    currState: State.TESTING,
    nextState: State.DONE,
    buttonDisplayText: 'Done!',
    backgroundColor: '#D4D4D4',
    color: 'black',
  },
  {
    currState: State.DONE,
    nextState: State.OPEN,
    buttonDisplayText: 'Reopen Task',
    backgroundColor: '#7BC17E',
    color: 'white',
  }
];

function swipeoutBtns(currItem, nextObj, changeState) {
  return [
    {
      text: nextObj.buttonDisplayText,
      onPress: () => {changeState(currItem, nextObj.nextState)},
      backgroundColor: nextObj.backgroundColor,
      color: nextObj.color,
    }
  ]
}

function getNextState(currState) {
  let retval = null;
  let index = STATES.findIndex(state => {
    return state.currState === currState
  });
  if (index > -1) {
    retval = STATES[index];
  }
  return retval;
}

const AgendaSectionList = ({agendas, completed, changeState, goToTaskScreen}) => {
  console.log('completed: ', JSON.stringify(completed));
  let filter;
  if (completed) {
    filter = checkStateCompleted;
  } else {
    filter = checkStatePending;
  }
  return (
    agendas.map((agenda, index) => {
      if (agenda.tasks.filter(filter).length !== 0) {
        const goalNameStyle = completed ? styles.goalNameCompleted : (index%2 !== 0 ?styles.goalName: styles.goalName2);
        return (
          <View style={styles.container}>
            <Text style={ [styles.commonGoalStyle, goalNameStyle] }>{agenda.parentName}</Text>
            <FlatList
              data={agenda.tasks.filter(filter)}
              renderItem={({item}) => SectionListItem({item, changeState, goToTaskScreen})}
              keyExtractor={(item, index) => item.key + index}
            />
         </View>
        )
      }

    })
  )
}

function checkStatePending(task) {
  return task.state !== State.DONE;
}

function checkStateCompleted(task) {
  return task.state === State.DONE;
}


const styles = StyleSheet.create({
  commonGoalStyle: {
    padding: 2,
    paddingHorizontal: 4,
    borderWidth:1,
    borderStyle:'solid',
    borderColor: '#D4D4D4'
  },
  goalName: {
    backgroundColor: '#FFF38B',
  },
  goalName2: {
    backgroundColor: '#58FEFE',
  },
  goalNameCompleted: {
    backgroundColor: '#7BC17E',
  },
  container: {
    flex: 1,
    marginHorizontal: 16,
    marginBottom: 10
  },
  item: {
    backgroundColor: "#ffffff",
    color: '#000000',
    padding: 10
  },
  header: {
    fontSize: 32,
    backgroundColor: "#fff"
  },
  title: {
    fontSize: 24
  }
});

const SectionListItem = ({ item, changeState, goToTaskScreen }) => (
  <View style={{marginLeft: 20, marginRight: 10, borderWidth:1, borderStyle:'solid', borderColor: '#D4D4D4'}}>
    <Swipeout style={{backgroundColor: 'white'}} autoClose={true} buttonWidth={100} right={swipeoutBtns(item, getNextState(item.state), changeState)}>
      <View style={{flexDirection: 'row'}}>
        <IconButton
        style={{flex: 1}}
        icon="arrow-expand"
        onPress={() => goToTaskScreen(item)} />
        <View style={{flex: 10}}>
          <Text style={styles.item}>{item.name}</Text>
        </View>
        <IconButton
        style={{flex: 1}}
        color="#D4D4D4"
        icon="arrow-expand-left" />

      </View>
    </Swipeout>
  </View>
);

export default AgendaSectionList;
