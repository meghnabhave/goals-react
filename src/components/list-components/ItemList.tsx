import React from 'react';

import { StyleSheet, FlatList, TouchableHighlight, View, TextInput} from 'react-native';
import PropTypes from 'prop-types';
import { IconButton, List, ProgressBar } from 'react-native-paper';

import DatabaseUtils from '../../database/DatabaseUtils';
import ItemType from '../../datamodel/Item';
import Swipeout from 'react-native-swipeout';
import { Place, State } from '../../datamodel/Item';
import ModalEditForm from './ModalEditForm';
import { Modal, Portal, Text, Button, Provider, Chip } from 'react-native-paper';


class ItemList extends React.Component {

  constructor( props ) {
    super(props);

    this.state = {
      isVisible: false,
      currItem: {}
    }
  }

  deleteItem(item) {
    DatabaseUtils.deleteItem(this.props.user.uid, item.key, item.parentKey);
  }

  changePlace(item) {
    let currentPlace = item.place;
    let newPlace = item.place === Place.BACKLOG ? Place.AGENDA : Place.BACKLOG;
    DatabaseUtils.updateItem(this.props.user.uid, item.key, {place: newPlace});
  }

  editItem(item) {
    this.setState({ isVisible: true, currItem: item});
  }

  renderRow(item) {
    return [
      <View style={styles.card}>
        <TouchableHighlight
            underlayColor = {'white'}
            onPress={handleGoalPress.bind(this, item)}
            style={styles.item}>
           <View style={styles.container}>
              <Text style={styles.itemName}>{item.name}</Text>
            </View>
        </TouchableHighlight>
        <View style={styles.chip}>
          <Chip mode='outlined' style={[styles.chipItem, item.state === State.DONE ? {backgroundColor: 'black'}: {}]} textStyle={item.state === State.DONE ? {color: 'white'}: {}}>{item.state}</Chip>
          { this.props.itemType === 'Tasks' ? (
            <Chip mode='outlined' style={[styles.chipItem, item.place === Place.AGENDA ? {borderColor: 'green', backgroundColor: 'green'}: {} ]} textStyle={item.place === Place.AGENDA ? {color: 'white'}: {}} onPress={() => { this.changePlace(item) }}>{item.place}</Chip>
          ): null}
          <IconButton size={15} style={[styles.chipItem, {marginRight: 20}]} icon="pencil-outline" onPress={() => { this.editItem(item) }} />
          <IconButton size={15} color={'#FF0000'} style={[styles.chipItem, {borderColor: '#FF0000'}]} icon="trash-can-outline" onPress={() => { this.deleteItem(item) }} />
        </View>
      </View>
    ];
  }

  render() {
    const itemType = this.props.itemType;
    return this.props.data.length==0? [
      <Text style={styles.helpfulMessage}>You haven't added any {itemType}! Add one now :)</Text>
    ]:[
        <FlatList
          keyExtractor={(item, index) => {return item.key;}}
          data={this.props.data}
          renderItem={({item}) => this.renderRow(item)}
        />
      ,
      <Provider>
        <Portal>
          <Modal
            visible={this.state.isVisible}
            onDismiss={()=> {this.setState({isVisible: false})}}>
            <ModalEditForm
              editDataHandler={editDataHandler.bind(this)}
              prevData={this.state.currItem}/>
          </Modal>
        </Portal>
     </Provider>

    ]
  }
}

function editDataHandler(item: Item) {
  this.setState({ isVisible:!this.state.isVisible})
  console.log(item);
  let update = this.state.currItem;
  update.name = item.name? item.name : update.name;
  update.state = item.state? item.state: update.state;
  this.setState({currItem: update})
  DatabaseUtils.updateItem(this.props.user.uid, this.state.currItem.key, item);
}

function handleLongPress() {
  console.log("List: Long presssed!");
}

function handleGoalPress(item) {
  // ItemDetails.js
  if( this.props.itemType !== 'Subtasks'){
    this.props.navigation.navigate(this.props.itemType, {
      item: item,
      itemType: this.props.itemSubtype,
      user: this.props.user
    });
  }

}

const styles = StyleSheet.create({
 menu: {
  flexDirection: 'row-reverse',
 },
 item: {
  flexDirection: 'row',
  backgroundColor: '#ffffff'
 },
 itemName: {
  fontSize: 20,
 },
 chip: {
  flexDirection: 'row',
  justifyContent: 'flex-end',
  marginTop: 10,
  marginBottom: 10
 },
 chipItem: {
   marginLeft: 3,
   marginRight: 3,
   borderStyle: 'solid',
   borderWidth: 1,
   borderColor: '#000000'
 },
 container: {
  flex: 1,
  backgroundColor: '#ffffff',
 },
 progress: {
  flex: 1
 },
 card: {
  flex:1,
  borderWidth: 1,
  borderRadius: 2,
  borderColor: '#ddd',
  borderBottomWidth: 0,
  backgroundColor: '#ffffff',
  shadowOffset: { width: 0, height: 2 },
  shadowOpacity: 0.8,
  shadowRadius: 2,
  elevation: 1,
  padding: 10,
  paddingBottom: 20,
  marginTop: 5,
  marginBottom: 10,
  marginLeft: 10,
  marginRight: 10,
 },
 modal: {
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor : "#ddd",
  height: 300 ,
  width: '80%',
  borderRadius:10,
  borderWidth: 1,
  borderColor: '#fff',
  marginTop: 80,
  marginLeft: 40,
 },
 text: {
    color: '#3f2949',
    marginTop: 10
 },
 helpfulMessage: {
   flex:1,
   paddingHorizontal: 30,
   color: 'grey'
 }
});

ItemList.propTypes = {
  itemType: PropTypes.string,
  itemSubtype: PropTypes.string,
  data: PropTypes.array,
  user: PropTypes.object,
  navigation: PropTypes.object,
}

export default ItemList;
