import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  TouchableHighlight,
  StyleSheet
} from 'react-native';

import { TextInput } from 'react-native-paper';


export const InputField = ({ placeholder, type, invalid, onChange }) => {
  return (
    <View
        style = { styles.inputView } >
      <TextInput
        mode= 'outlined'
        autoCompleteType={ type }
        secureTextEntry= { type === 'password' }
        theme={{colors: {primary: '#4D4E4F'}}}
        style={ styles.input }
        placeholder={ placeholder }
        autoCapitalize='none'
        error ={ invalid }
        onChangeText={
          text => onChange(text)
        }
      />
    </View>
  )
}

export const Button = ({type, isPrimary, onPress}) => {
  let buttonLabel = '';
  switch(type) {
    case 'login':
      buttonLabel = 'LOGIN'
      break;
    case 'signup':
      buttonLabel = 'SIGNUP'
      break;
  }

  const style = isPrimary ?
  {button: styles.primaryButton, text: styles.primaryText} :
  {button: styles.secondaryButton, text: styles.secondaryText}

  return (
    <TouchableOpacity
      style={[styles.primaryButton, style.button] }
      onPress= { () => onPress() }
    >
    <Text style={style.text}>{ buttonLabel }</Text>
    </TouchableOpacity>
  )
}

export const ErrorMessage = ({message}) => {
    return (
      <Text style={{color: '#bb0000', paddingTop: 10}}>{message}</Text>
    );
}

export const ForgotPassword = ({onPress}) => {
  return (
    <Text
      style={[styles.linkText,{color: 'red'}]}
      onPress={() => {onPress()}}>Forgot Password?</Text>
  )
}

export const LoginForm = ({emailInvalid, onChangeEmail, passwordInvalid, onChangePassword,
  errorMessage, onLoginPress, onForgotPasswordPress, onSignupPress}) => {
    return (
      <View style={ styles.form } >
          <InputField
            placeholder='Email...'
            type='email'
            invalid={ emailInvalid }
            onChange = { text => onChangeEmail(text)}
          />
          <InputField
            placeholder='Password...'
            type='password'
            invalid={ passwordInvalid }
            onChange = { text => onChangePassword(text)}
          />
        <ErrorMessage message={errorMessage} />
        <Button
          type='login'
          isPrimary={true}
          onPress={() => onLoginPress()}
        />
        <ForgotPassword
          onPress={() => onForgotPasswordPress()} />

        <Text style={styles.linkText}>Dont have an account yet?</Text>
        <Button
          type='signup'
          isPrimary={false}
          onPress={() => onSignupPress()} />
      </View>
    )

}

export const SignupForm = ({onChangeName, emailInvalid, onChangeEmail, passwordInvalid, onChangePassword,
  errorMessage, onSignupPress, onLoginPress}) => {
  return (
    <View style={ styles.form }>
      <InputField
        type='name'
        invalid={false}
        placeholder='Name...'
        onChange={text => onChangeName(text)} />

      <InputField
        type='email'
        invalid={emailInvalid}
        placeholder='Email...'
        onChange={text => onChangeEmail(text)} />

      <InputField
        type='password'
        invalid={passwordInvalid}
        placeholder='Password...'
        onChange={(text) => onChangePassword(text)} />

      <ErrorMessage message={errorMessage} />

      <Button
        type='signup'
        isPrimary={true}
        onPress={() => onSignupPress()} />

      <Text style={styles.linkText}>Already have an account?</Text>

      <Button
        type='login'
        isPrimary={false}
        onPress={() => onLoginPress()} />
    </View>
  );
}

const styles = StyleSheet.create({
  form : {
    flex: 1,
    backgroundColor: '#ffffff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  inputView : {
    width:"80%"
  },

  input : {
    height:50,
    backgroundColor: "#ffffff",
    borderColor:"#ACA5A5",
    fontSize: 15

  },

  primaryButton : {
    width:"80%",
    backgroundColor:"#DF2E2E",
    borderRadius:5,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:20,
    marginBottom:10,
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOpacity: 0.8,
    elevation: 6,
  },

  primaryText : {
    color: "#ffffff",
    fontSize: 20
  },

  linkText : {
    color: '#ACA5A5',
    marginTop: 10
  },

  secondaryButton: {
    backgroundColor: '#f0f0f0',
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOpacity: 0.8,
    elevation: 6,
  },
  secondaryText: {
    color: '#4D4E4F',
    fontSize: 15
  }
})
