import React from 'react';

import DatabaseUtils from '../database/DatabaseUtils';

import { SignupForm } from '../components/LoginComponent';

class SignupScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      name:'',
      email: '',
      password: '',
      error: '',
      emailInvalid: '',
      passwordInvalid: ''
    }
  }


  render() {
    return (
      <SignupForm
        onChangeName={text => this.setState({ name: text })}
        emailInvalid={this.state.emailInvalid}
        onChangeEmail={text => this.setState({ email: text })}
        passwordInvalid={this.state.passwordInvalid}
        onChangePassword={text => this.setState({ password: text })}
        errorMessage={this.state.error}
        onSignupPress={onSignupPress.bind(this)}
        onLoginPress={() => this.props.navigation.navigate('Login')}
      />
    );
  }
}

function onSignupPress() {
  let that = this;
  DatabaseUtils.createUserWithEmailAndPassword(this.state.email.trim(), this.state.password.trim())
  .then((user) => {
    let id: string = user.user.uid;
    let newUser = {
        username: this.state.name,
        email: user.user.email,
        goals: {},
        tasks: {},
    };
    // DatabaseUtils.updateProfile({
    //     displayName: this.state.name
    // });
    DatabaseUtils.addUserToRealtimeDatabase(id, newUser);
    this.props.navigation.navigate('Agenda', {user});
  })
  .catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    // ...
    console.log('in createUserWithEmailAndPassword: ' + errorMessage);

    var message = 'Error while logging in.';
    var emailInvalid = false;
    var passwordInvalid = false;
    switch(errorCode) {
      case 'auth/email-already-in-use':
        message = 'This email address is already in use! Login now.';
        emailInvalid = true;
        break;
      case 'auth/invalid-email':
        message = 'This email address is not valid.';
        emailInvalid = true;
        break;
      case 'auth/operation-not-allowed':
        message = 'This email and password is disabled. Please contact suppport.';
        emailInvalid = true; passwordInvalid = true;
        break;
      case 'auth/weak-password':
        message = 'Your password is too weak. Try a stronger password with 8 or more characters.';
        passwordInvalid = true;
    }

    that.setState({
      error: message,
      emailInvalid: emailInvalid,
      passwordInvalid: passwordInvalid
    });
  });
}

export default SignupScreen;
