import React from 'react';
import { Text, View, ScrollView, BackHandler, ToastAndroid } from 'react-native';
import { IconButton } from 'react-native-paper';
import { Item, ItemType, State, Place } from '../datamodel/Item';
import DatabaseUtils from '../database/DatabaseUtils';
import BottomNavigationComponent from '../components/BottomNavigationComponent';
import AgendaSectionList from '../components/list-components/AgendaSectionList';

import { connect } from 'react-redux';
import { watchUserData } from '../redux/app-redux';

const mapStateToProps = (state) => {
  return {
    agendas: getAgendas(state.userData.list)
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    watchUserData: (uid) => dispatch(watchUserData(uid))
  };
}

class AgendaScreen extends React.Component {

  constructor(props) {
    super(props);
    this.props.watchUserData(this.props.route.params.user.uid);

    this.props.navigation.setOptions({
      headerLeft: () => (null),
      headerRight: () => (
        <IconButton
          icon="account-circle-outline"
          size={20}
          onPress={() => this.props.navigation.navigate('Profile', {
            user : this.props.route.params.user
          })}
        />
      ),
    });
  }

  render() {
    return [
      <ScrollView>
        <View style={{alignItems: 'center'}}>
          <Text style={{fontWeight: 'bold', fontSize: 20, paddingBottom: 10}}>Agenda</Text>
          <Text style={{fontSize: 15, paddingBottom: 10}}>this sprint</Text>
      </View>
        <AgendaSectionList
          agendas={this.props.agendas}
          completed={false}
          changeState={this.changeState.bind(this)}
          goToTaskScreen={this.goToTaskScreen.bind(this)}
        />
        <View style={{alignItems: 'center'}}>
          <Text style={{fontWeight: 'bold', fontSize: 20, paddingBottom: 10}}>Completed</Text>
          <Text style={{fontSize: 15, paddingBottom: 10}}>current sprint</Text>
        </View>
        <AgendaSectionList
          agendas={this.props.agendas}
          completed={true}
          changeState={this.changeState.bind(this)}
          goToTaskScreen={this.goToTaskScreen.bind(this)}
        />
        <View style={{height: 50}}><Text></Text></View>
      </ScrollView>,
      <BottomNavigationComponent
        navigation={this.props.navigation}
        user={this.props.route.params.user}
       />
    ]
  }

  // Handlers
  changeState(currItem, nextState) {
    let update = {state: currItem.state};
    update.state = nextState;
    this.updateState(currItem.parentKey, currItem.key, update);
    DatabaseUtils.updateItem(this.props.route.params.user.uid, currItem.key, update);
    // this.runJSInBackground('DatabaseUtils.updateItem(this.props.route.params.user.uid, currItem.key, update);')
  }


  updateState(parentKey, taskKey, update) {
      let agendas = this.props.agendas;
      let agendaIndex = agendas.findIndex(agenda => agenda.parentKey === parentKey);
      let agenda = agendas[agendaIndex];
      let taskIndex = agenda.tasks.findIndex(task => task.key === taskKey);
      let task = agenda.tasks[taskIndex];

      agendas[agendaIndex].tasks[taskIndex].state = update.state;

      this.setState({agendas: agendas});
  }

  goToTaskScreen(task) {
    this.props.navigation.navigate('Tasks', {
      item: task,
      itemType: 'Subtasks',
      user: this.props.route.params.user
    });
  }
}

function getAgendas(list) {
  var returnArr = [];
  var allItems = [];
  list.forEach(function(item) {
    allItems.push(item);
    if(item.place === Place.AGENDA && item.type === ItemType.TASK) {
      returnArr.push(item);
    }
  });

  var retArr = [];
  returnArr.forEach((item) => {
    if(item.type === ItemType.TASK) {
      let parent = allItems.filter((it) => item.parentKey === it.key);
      item.parentName = parent[0].name;
    }

    let index = retArr.findIndex(i => i.parentKey === item.parentKey);

    if (index < 0) {
      let newObj = {
        parentKey: item.parentKey,
        parentName: item.parentName,
        tasks: []
      };
      newObj.tasks.push(item);
      retArr.push(newObj);
    } else {
      retArr[index].tasks.push(item);
    }

  });

  return retArr;
}

export default connect(mapStateToProps, mapDispatchToProps)(AgendaScreen);
