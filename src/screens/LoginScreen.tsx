import React from 'react';

import DatabaseUtils from '../database/DatabaseUtils';

import { LoginForm } from '../components/LoginComponent';

class LoginScreen extends React.Component {

  state : {
    email : string,
    password : string
  }

  constructor( props ) {
    super(props);

    this.state = {
      email: 'Email',
      password: 'Password',
      error: '',
      emailInvalid: false,
      passwordInvalid: false
    };
  }

  render() {
    return (
      <LoginForm
        emailInvalid={this.state.emailInvalid}
        onChangeEmail={ text => this.setState({email: text})}
        passwordInvalid={this.state.passwordInvalid}
        onChangePassword={ text => this.setState({ password: text })}
        errorMessage={this.state.error}
        onLoginPress={onLoginPress.bind(this)}
        onForgotPasswordPress={() => {this.props.navigation.navigate('ForgotPassword')}}
        onSignupPress={onSignupPress.bind(this)}
      />
    )
  }

}

function onLoginPress() {

    var that = this;
    DatabaseUtils.signInWithEmailAndPassword(this.state.email.trim(), this.state.password.trim())
    .then(user => {
        if (user) {
          this.props.navigation.navigate('Agenda', {user: user.user});
        }
    }).catch(function(this,error){
      // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // ...
        console.log('In sign in with email and password: ', errorCode, errorMessage);
        var message = 'Error while logging in.';
        var emailInvalid = false;
        var passwordInvalid = false;
        switch(errorCode) {
          case 'auth/invalid-email':
            message = 'The email is invalid';
            emailInvalid = true;
            break;
          case 'auth/user-disabled':
            message = 'This account has been disabled. Please contact support.';
            emailInvalid = true; passwordInvalid = true;
            break;
          case 'auth/user-not-found':
            message = 'You don\'t have an account! Sign up now.';
            emailInvalid = true; passwordInvalid = true;
            break;
          case 'auth/wrong-password':
            message = 'Your password is incorrect.';
            passwordInvalid = true;
        }

        that.setState({
          error: message,
          emailInvalid: emailInvalid,
          passwordInvalid: passwordInvalid
        });
    })

}

function onSignupPress() {
  this.props.navigation.navigate('Signup');
}

export default LoginScreen;
