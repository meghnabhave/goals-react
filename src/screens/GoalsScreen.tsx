import React from 'react';
import {
  Text,
  View,
  StyleSheet
} from 'react-native';

import { IconButton } from 'react-native-paper';
import ItemDetailsScreen from '../components/detail-view/ItemDetailsScreen';

class GoalsScreen extends React.Component {

  constructor(props) {
    super(props);
    this.props.navigation.setOptions({
      headerRight: () => (
        <IconButton
          icon="account-circle-outline"
          size={20}
          onPress={() => this.props.navigation.navigate('Profile', {
            user : this.props.route.params.user
          })}
        />
      ),
    });
  }

  render() {
    return [
      <View style={{ padding:10,margin: 10, height:100, backgroundColor: '#ffffff',}}>
        <Text style={{fontSize: 40}}>{this.props.route.params.item.name}</Text>
      </View>,
      <ItemDetailsScreen
        itemType='Tasks'
        itemSubtype='Subtasks'
        item={this.props.route.params.item}
        user= {this.props.route.params.user}
        navigation={this.props.navigation} />
    ]
  }
}

export default GoalsScreen;
