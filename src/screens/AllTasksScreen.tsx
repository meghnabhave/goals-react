import React from 'react';
import { Text, StyleSheet, View, ScrollView} from 'react-native';

import ItemList from '../components/list-components/ItemList';
import { Divider, Subheading, IconButton } from 'react-native-paper';
import { ItemType } from '../datamodel/Item';

import { connect } from 'react-redux';
import { watchUserData } from '../redux/app-redux';

const mapStateToProps = (state) => {
  return {
    tasks: getTasksFromUserData(state.userData.list)
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    watchUserData: (uid) => dispatch(watchUserData(uid))
  };
}

class AllTasksScreen extends React.Component {


  constructor(props) {
    super(props);
    this.props.watchUserData(this.props.route.params.user.uid);

    this.props.navigation.setOptions({
      headerRight: () => (
        <IconButton
          icon="account-circle-outline"
          size={20}
          onPress={() => this.props.navigation.navigate('Profile', {
            user : this.props.route.params.user
          })}
        />
      ),
    });
  }

  render() {
    return this.props.tasks === null? (<Text>Waiting for data</Text>):
    (
      <View style={styles.container}>
        <Subheading style={{alignSelf: 'center', paddingTop: 10}}>All Tasks</Subheading>
        <ScrollView>
        {this.props.tasks.map((task) => {
         return (
           <View style={{
             paddingVertical: 5
           }}>
            <Divider />
             <Text style={styles.sectionHeader}>{ task.goalName }</Text>
             <View>
               <ItemList
                 itemType='Tasks'
                 itemSubtype='Subtasks'
                 data={task.tasklist}
                 user={this.props.route.params.user}
                 navigation={this.props.navigation}/>
            </View>
           </View>
          );
        })}
       </ScrollView>
      </View>

    )
  }

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    padding: 10

  },

  sectionHeader: {
    fontSize: 20,
    padding: 5,
  }
})

function getTasksFromUserData(list) {
  // setProgress(snapshot);

  var tasks = [];
  // var goalKeyNameMapper = new Map();
    list.forEach(function(item) {

      if(item.type === ItemType.GOAL){
        // goalKeyNameMapper.set(item.key, item.name);
        var objToPush = {
          goalKey: item.key,
          goalName: item.name,
          tasklist: []
        }
        tasks.push(objToPush);
      }
    });

    list.forEach(function(item){

      if(item.type === ItemType.TASK) {

        let goalKey = item.parentKey;
        // let goalName = goalKeyNameMapper.get(goalKey);

        tasks.forEach(function(task) {
            if(task.goalKey === goalKey) {
              task.tasklist.push(item)
            }
          }
        );
      }
    });
  return tasks;
}

export default connect(mapStateToProps, mapDispatchToProps)(AllTasksScreen);
