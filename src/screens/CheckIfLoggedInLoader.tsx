import React from 'react';
import { View } from 'react-native';
import { ActivityIndicator, Colors } from 'react-native-paper';
import DatabaseUtils from '../database/DatabaseUtils';

export default class CheckIfLoggedInLoader extends React.Component {

  componentDidMount = () => {
      DatabaseUtils.checkIfAlreadyLoggedInAndNavigateToDashboard(this.props.navigation);
  }

  render() {
    return (
      <View style={{flex:1,
        alignItems:'center',
        justifyContent:'center'}}>
        <ActivityIndicator animating={true} color={'#DF2E2E'} />
      </View>
    )
  }
}
