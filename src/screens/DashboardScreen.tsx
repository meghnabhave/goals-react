import React from 'react';
import {
  Text,
  View,
  StyleSheet
} from 'react-native';

import { Card, Button, Icon, IconButton } from 'react-native-paper';

import DatabaseUtils from '../database/DatabaseUtils';
import AddItemButton from '../components/add-item/AddItemButton';
import ItemList from '../components/list-components/ItemList';
import ItemDetailsScreen from '../components/detail-view/ItemDetailsScreen';

class DashboardScreen extends React.Component {

  constructor(props) {
    super(props);
    this.props.navigation.setOptions({
      headerRight: () => (
        <IconButton
          icon="account-circle-outline"
          size={20}
          onPress={() => this.props.navigation.navigate('Profile', {
            user : this.props.route.params.user
          })}
        />
      ),
    });
    if ( this.props.route.params.user){
      this.state = {
        user: this.props.route.params.user,
        loading: false
      }
    } else {
      this.state.loading = true;
    }
  }


  render() {
    const loading = this.state.loading;
    // console.log(this.state.user.uid);
    return loading ? [
      <Text style={styles.baseText}>loading...</Text>
    ] :
    [
      <ItemDetailsScreen
        itemType='Goals'
        itemSubtype='Tasks'
        user= {this.state.user}
        navigation={this.props.navigation}
      />
    ];
  }

}

const styles = StyleSheet.create({
  baseText: {
    margin: 10,
    padding: 10,
    justifyContent: 'center'
  },
});

export default DashboardScreen;
