import React from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity
} from 'react-native';

import { TextInput } from 'react-native-paper';
import DatabaseUtils from '../database/DatabaseUtils';

class ForgotPasswordScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      email: 'Email',
      error: '',
      emailInvalid: false,
    }
  }

  render() {
    return (
      <View
          style={ styles.form }
        >
        <View
            style = { styles.inputViewEmail }
          >
          <TextInput
            mode= 'outlined'
            autoCompleteType='email'
            theme={{colors: {primary: '#4D4E4F'}}}
            style={ styles.input }
            placeholder='Email ..'
            autoCapitalize='none'
            error ={this.state.emailInvalid}
            onChangeText={
              text => this.setState({ email: text })
            }
          />
          <TouchableOpacity
            style={ styles.button }
            onPress={() => {
              console.log(this.state.email);
              handlePasswordReset(this.state.email.trim());
            }}
          >
          <Text style={styles.loginText}>Send Email</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

function handlePasswordReset(email) {

    DatabaseUtils.forgotPassword(email)
    .then(function() {
      alert('Password Reset Email Sent. Reset password and login again.');
      // this.props.navigation.navigate('Login')
    })
    .catch(function(error) {
      console.log('error: ', error);
    })
}

const styles = StyleSheet.create({
  form : {
    flex: 1,
    backgroundColor: '#ffffff',
    alignItems: 'center',
    paddingTop: 100,
  },

  inputViewEmail : {
    width:"80%"
  },
  input : {
    height:50,
    backgroundColor: "#ffffff",
    borderColor:"#ACA5A5",
    fontSize: 15

  },
  button : {
    width:"100%",
    backgroundColor:"#DF2E2E",
    borderRadius:5,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:20,
    marginBottom:10,
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOpacity: 0.8,
    elevation: 6,
  },
  loginText : {
    color: "#ffffff",
    fontSize: 20
  },
})

export default ForgotPasswordScreen;
