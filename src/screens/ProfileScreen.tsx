import React, { useCallback } from 'react';
import { Text, View, TouchableOpacity, StyleSheet, Linking, Alert } from 'react-native';

import { connect } from 'react-redux';
import { watchUserData } from '../redux/app-redux';

import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';
import DatabaseUtils from '../database/DatabaseUtils';

const LeftContent = props => <Avatar.Icon {...props} icon="account-circle-outline" theme={{colors:{primary: '#B8B9BB'}}}/>
const openURL = (url) => {Linking.openURL(url).catch((err) => console.error('An error occurred', err));}

const PRIVACY_POLICY_URL = "https://www.meghnabhave.com/goals-privacy-policy";

const OpenURLButton = ({ url, title }) => {
  const handlePress = useCallback(async () => {
    // Checking if the link is supported for links with custom URL scheme.
    const supported = await Linking.canOpenURL(url);

    if (supported) {
      // Opening the link with some app, if the URL scheme is "http" the web link should be opened
      // by some browser in the mobile
      await Linking.openURL(url);
    } else {
      Alert.alert(`Don't know how to open this URL: ${url}`);
    }
  }, [url]);

  return <Button style={styles.button} color={'#000000'} mode='outlined' onPress={handlePress}>{title}</Button>;
}

const mapStateToProps = (state) => {
  return {
    email: state.userData.email,
    username: state.userData.username,
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    watchUserData: (uid) => dispatch(watchUserData(uid))
  };
}

class ProfileScreen extends React.Component {

  constructor(props) {
    super(props);
    this.props.watchUserData(this.props.route.params.user.uid);
  }

  render() {
    return (
      <View style={styles.container}>
      <Card>
        <Card.Title title={this.props.username} subtitle={this.props.email} left={LeftContent} />
      </Card>
      <Button
        style={[styles.button,{backgroundColor: '#FF0000'}]}
        color={'#FFFFFF'}
        mode='outlined'
        onPress={() => DatabaseUtils.signOut(this.props.navigation)}>
        Logout
      </Button>
      <OpenURLButton url={PRIVACY_POLICY_URL} title='Privacy Policy'/>
    </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
    backgroundColor: '#ffffff',
  },
  button: {
    width:"80%",
    backgroundColor:"#fefefe",
    borderRadius:5,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:20,
    marginBottom:10,
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOpacity: 0.8,
    elevation: 6,
    alignSelf: 'center'
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);
