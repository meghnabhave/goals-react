import * as firebase from 'firebase';

// Initialize Firebase
const firebaseConfig = {
  apiKey: "AIzaSyCWM9rfAz9dyv-f0O36CSpguFMWRjtQ8NY",
  authDomain: "goals-16f9d.firebaseapp.com",
  databaseURL: "https://goals-16f9d.firebaseio.com",
  projectId: "goals-16f9d",
  storageBucket: "goals-16f9d.appspot.com",
  messagingSenderId: "233616218033",
  appId: "1:233616218033:web:91fd240861ced1c137cf90",
  measurementId: "G-66ST0EFH60"
};

export const Firebase = firebase.initializeApp(firebaseConfig);

export const usersRef = Firebase.database().ref('users');
export const auth = Firebase.auth();

// export usersRef;
